<!-- SPDX-FileCopyrightText: Rick van Rein <rick@openfortress.nl>
     SPDX-License-Identifier: CC-BY-SA-3.0
-->

# Quick DER precompiled ASN.1 code

> *Many standards specifications define ASN.1 fragments.
> We try to deliver them to you in shrink-wrapped form.*

Ideally, in Python all you would do to use ASN.1 from a
given standard would be something like

```
from quickder.rfc import rfc4511
```

Similarly, in C all you would do to use the same standard
would be

```
#include <quickder/rfc4511>
```

The Quick DER package offers just that.  It comes with
packages like `quickder.rfc` for Python and `quickder-rfc`
for C.  In both cases a namespace for specifications
surrounds the information.  Multiple packages can add
their pre-compiled things here.

Have a look in the [rfc directory](rfc/) for an idea
of how this is done.


