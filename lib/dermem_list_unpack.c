/*
 *  SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

/* Given a dernode in wire format, as parsing of SEQUENCE OF
 * or SET OF would give, turn it into info form, where each
 * element is a dernode holding the wire format of the element.
 *
 * TODO: both directions are useful.  put both below.
 */

//TODO//
//TODO// /* Unpack a list from a parsed SEQUENCE OF or SET OF into an
//TODO//  * array of the structure of an individual element.  Allocate
//TODO//  * the memory in the provided memory pool.
//TODO//  */
//TODO// bool dermem_list_unpack (void *master, dercursor wirelist,
//TODO// 					derwalk *syntax_one, size_t size_one,
//TODO// 					dercursor **out_all, size_t *outcount) {
//TODO// 	size_t num = der_count (&wirelist);
//TODO// 	if (!dermem_alloc (size_one * num * sizeof (dercursor), out_all)) {
//TODO// 		/* errno is ENOMEM */
//TODO// 		return false;
//TODO// 	}
//TODO// 	*outcount = num;
//TODO// 	...TODO...
//TODO// }
