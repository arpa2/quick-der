/*
 *  SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdbool.h>

#include <sys/types.h>
#include <errno.h>

#include <arpa2/quick-dermem.h>
#include <arpa2/quick-der.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>

#include "der-dbg.h"


/* Allocate memory, der_pack() a structure into it and send
 * it off over the given socket.
 */
bool dermem_send (void *master, const int sox, const derwalk *syntax, const struct dercursor *in_cursors) {
	struct dercursor packed;
	log_debug ("dermem_send() on sox=%d", sox);
	memset (&packed, 0, sizeof (packed));
	if (!dermem_pack (master, syntax, in_cursors, &packed)) {
		/* errno is set by dermem_pack() */
		log_error ("!dermem_pack()");
		return false;
	}
	print_block ("dermem_send() bytes", packed.derptr, packed.derlen, false);
	ssize_t iolen = send (sox, packed.derptr, packed.derlen, 0);
	if (iolen < (ssize_t) packed.derlen) {
		if (iolen >= 0) {
#ifdef ECOMM
			errno = ECOMM;
#else
			errno = EPIPE;
#endif
		}
		log_error ("iolen < packed.derlen");
		return false;
	}
	return true;
}
