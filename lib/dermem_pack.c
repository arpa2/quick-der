/*
 *  SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdbool.h>

#include <arpa2/quick-dermem.h>
#include <arpa2/quick-der.h>
#include <arpa2/except.h>


/* Allocate memory and der_pack() a structure into it.
 *
 * Before calling, out_packed->derptr must be NULL.
 */
bool dermem_pack (void *master, const derwalk *syntax, const dercursor *in_cursors,
				dercursor *out_packed) {
	size_t packlen = der_pack (syntax, in_cursors, NULL);
	if (packlen == 0) {
		errno = EBADMSG;
		log_error ("der_pack() == 0");
		return false;
	}
	if (!dermem_alloc (master, packlen, (void **) &out_packed->derptr)) {
		/* errno is ENOMEM */
		log_error ("!dermem_alloc ()");
		return false;
	}
	out_packed->derlen = packlen;
	der_pack (syntax, in_cursors, out_packed->derptr + packlen);
	return true;
}
