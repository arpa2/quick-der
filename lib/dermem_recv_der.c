/*
 *  SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdbool.h>

#include <sys/types.h>
#include <errno.h>

#include <arpa2/quick-dermem.h>
#include <arpa2/quick-der.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>

#include "der-dbg.h"


/* Receive one DER structure over a given socket and unpack.
 * The memory for receiving the structure is assumed to exist,
 * and will be setup as needed.  The data memory for the DER
 * blob is allocated in the master's pool.
 *
 * It assumed that 5 bytes can be read effortlessly.  No more
 * that 16 MB can be read at a time.
 */
bool dermem_recv_der (void *master, const int sox, const derwalk *syntax, struct dercursor *out_cursors,
					struct dercursor *opt_out_der) {
	uint8_t intro [5];
	log_debug ("dermem_recv() on sox=%d", sox);
	ssize_t iolen = recv (sox, intro, 5, 0);
	log_debug ("dermem_recv() got the %zd byte intro", iolen);
	if (iolen < 5) {
		if (iolen >= 0) {
			errno = EPIPE;
		}
		log_error ("iolen < 5");
		return false;
	}
	uint8_t tag;
	size_t len;
	uint8_t hlen;
	struct dercursor startcrs = { .derptr = intro, .derlen = iolen };
	if (der_header (&startcrs, &tag, &len, &hlen) != 0) {
		log_error ("der_header()");
		return false;
	}
	log_debug ("dermem_recv() tag 0x%02x hlen=%d, len=%zd", tag, hlen, len);
	if (hlen + len < 5) {
		/* violation of assumption */
		errno = EBADMSG;
		log_error ("hlen + len < 5");
		return false;
	}
	uint8_t *derblob = NULL;
	if (!dermem_alloc (master, hlen + len, (void **) &derblob)) {
		/* errno is ENOMEM */
		log_error ("dermem_alloc()");
		return false;
	}
	log_debug ("dermem_recv() allocated the desired %zd bytes at %p", hlen + len, derblob);
	memcpy (derblob, intro, iolen);
	size_t ioofs = iolen;
	while (ioofs < hlen + len) {
	log_debug ("dermem_recv() tries to get from %zd by adding %zd at %p", ioofs, hlen + len - ioofs, derblob + ioofs);
		iolen = recv (sox, derblob + ioofs, hlen + len - ioofs, 0);
		if (iolen <= 0) {
			if (iolen == 0) {
				errno = EPIPE;
			}
			log_error ("iolen <= 0");
			return false;
		}
		ioofs += iolen;
	}
	print_block ("dermem_recv() bytes", derblob, ioofs, false);
	struct dercursor blobcrs = { .derptr = derblob, .derlen = ioofs };
	if (opt_out_der != NULL) {
		*opt_out_der = blobcrs;
	}
	if (der_unpack (&blobcrs, syntax, out_cursors, 1) != 0) {
		/* errno has been set */
		log_error ("der_unpack()");
		return false;
	}
	return true;
}
