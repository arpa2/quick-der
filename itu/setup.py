#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Rick van Rein <rick@openfortress.nl>

#TODO# Would be generate to have the .py elsewhere
#TODO# Is generation path-relative, same-dir, or here?


from setuptools import setup, Extension, find_packages
from sys import executable, exit
from os import path, environ, listdir, unlink, makedirs
from os.path import exists

#
# We require an installed asn2quickder package
# and will approach the script directly.
#
from arpa2.quickder_tools import asn2quickder

here = (path.dirname(path.realpath(__file__)))
base = (path.dirname(here))

#
# Build things in build/quickder
#
workdir = path.join (here, 'build', 'quickder')
if not exists (workdir):
	makedirs (workdir)

lshere = { fn:path.getmtime (path.join (here,    fn)) for fn in listdir (here   ) if '.' in fn and fn [:1] != '.' }
lswork = { fn:path.getmtime (path.join (workdir, fn)) for fn in listdir (workdir) if '.' in fn and fn [:1] != '.' }

#
# Map the .asn1 files in this directory to .py files
#
__all__ = [ ]
for asn1f in lshere.keys ():
	if asn1f [-5:] != '.asn1':
		continue
	__all__.append (asn1f [:-5])
	pyfil = asn1f [:-5] + '.py'
	src = lshere.get (asn1f, None)
	dst = lswork.get (pyfil, 0   )
	if dst > src:
		print ('Reusing mapping of %s to Python' % asn1f)
		continue
	cmd = [ 'itu/setup.py',
			'-l', 'python',
			'-I', here,
			'-O', workdir,
			path.join (here, asn1f) ]
	print ('Mapping %s to Python' % asn1f)
	try:
		asn2quickder.main (cmd)
	except:
		unlink (pyfil)
		raise

open (path.join (workdir, '__init__.py'), 'w').write ('__all__ = %r\n' % __all__)


#
# Construct package -- Quick DER -- quickder.itu
#
readme = open (path.join (base, 'PRECOMPILED.MD')).read()

setup(
	# What?
	name = 'quickder.itu',
	license = 'BSD-2',
	description = 'Prebuilt Quick DER includes based on ASN.1 in RFCs',
	long_description = readme,
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/arpa2/quick-der',
	version='1.4.0',

	# Who?
	author = 'Rick van Rein',
	author_email = 'rick@openfortress.nl',

	# Where?
	namespace_packages = [ 'quickder' ],
	packages = [ 'quickder', 'quickder.itu' ],
	package_dir = {
		'quickder'    : path.join (base, 'python'),
		'quickder.itu': workdir,
	},
	package_data = {
		# 'quickder.itu' : [ path.join (base, 'PRECOMPILED.MD'),
		# ],
	},

	# Requirements
	install_requires=[ 'arpa2.quickder', ],
)

