#!/usr/bin/env python

#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>

import sys
# ../../python is (once this test is being run) the source-dir python,
#    ../python is inside the build-directory
sys.path = ['../../python/testing', '../python/testing'] + sys.path

from rfc4511 import LDAPMessage

lm = LDAPMessage()

print(lm)
