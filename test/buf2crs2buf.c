/* test buf2crs, crs2buf and pointer variants */

/*
 *  SPDX-FileCopyrightText: Copyright 2019, Rick van Rein <rick@openfortress.nl>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#define DEBUG 1

#include <stdlib.h>
#include <assert.h>


#include <arpa2/quick-mem.h>
#include <arpa2/quick-dermem.h>


int main (int argc, char *argv []) {
	for (int argi = 0; argi < argc ; argi++) {
		char *argptr = argv [argi];
		int arglen = strlen (argptr);
		// b0 --> c1 --> b2
		membuf b0;
		b0.bufptr = argptr;
		b0.buflen = arglen;
		dercursor c1 = buf2crs (b0);
		membuf b2 = crs2buf (c1);
		assert (c1.derlen == arglen);
		assert (b2.buflen == arglen);
		assert (0 == strncmp (c1.derptr, argptr, c1.derlen));
		assert (0 == strncmp (b2.bufptr, argptr, b2.buflen));
		// b0p --> c1p --> b2p
		membuf *b0p;
		b0p = &b0;
		dercursor *c1p = buf2crsp (b0p);
		membuf *b2p = crs2bufp (c1p);
		assert (c1p->derlen == arglen);
		assert (b2p->buflen == arglen);
		assert (0 == strncmp (c1p->derptr, argptr, c1p->derlen));
		assert (0 == strncmp (b2p->bufptr, argptr, b2p->buflen));
	}
}
