/** @ingroup der
 * @{
 * @defgroup dermem Quick DER pooled memory management
 * @{
 *
 * This is a generic library for memory pooling.  Quick DER
 * itself does not allocate or free memory, but this set of
 * routines can help to make it more flexible.  It will not
 * be integrated into mainstream libquickder.so but could
 * be provided in another library.
 *
 * A few secure techniques are supported:
 *  - pools simplify referential integrity
 *  - pools can help to avoid memory leakage
 *  - memory is delivered in zeroed state
 *  - memory is zeroed before freeing it
 *  - references must be NULL to allocate to them
 *  - references are set to NULL when they are freed
 *
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2016-2021, Rick van Rein <rick@openfortress.nl>
 *  SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef QUICKDER_MEM_H
#define QUICKDER_MEM_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-mem.h>




/********** TYPE WRAPPERS FOR QUICK-DERMEM **********/


#ifdef DERMEM_SIZE_T
#error "Do not define DERMEM_SIZE_T but MEM_SIZE_T; then, build both Quick-MEM and Quick-DERMEM"
#endif

typedef mem_size_t dermem_size_t;


/* Transform the membuf type to a dercursor, or back.  The types
 * are the same, but their usage area has caused us to use
 * different names.  Mapping them is trivial.
 */
/** @def buf2crs(b)
 * @brief Map a dermem buffer @a b to a dercursor.
 */
/** @def buf2crsp(pb)
 * @brief Map a dermem buffer pointer @a pb to a dercursor pointer.
 */
/** @def crs2buf(c)
 * @brief Map a dercursor @a c to a dermem buffer.
 */
/** @def crs2bufp(pc)
 * @brief Map a dercursor pointer @a pc to a dermem buffer pointer.
 */
#define buf2crs(b) (* (dercursor *) &(b))
#define buf2crsp(pb) ((dercursor *) (pb))
#define crs2buf(c) (* (membuf *) &(c))
#define crs2bufp(pc) ((membuf *) (pc))


/** @brief Allocate a new memory pool.
 *
 * The memory pool is identified by an application
 * master data structure from which the pool hangs.  When
 * this master data is cleared, so should the pool be,
 * through a call to dermem_close().
 *
 * The master pointer is always used explicitly in that
 * role in dermem_alloc() and dermem_close(), which means
 * that it need not be unique.  As a result, you can call
 * dermem_open() asking for 0 bytes of space.  The master
 * pointer will not be unique, but its use in its role is.
 *
 * The pool does not have to be linked explicitly from the
 * appdata; the appdata is simply the first allocated bit
 * in the data structure, and offset calculations suffice.
 *
 * This function fails fatally if *master is not NULL.
 *
 * This function returns true on success, false on failure.
 * When this function fails, it sets errno to ENOMEM.
 */
// static inline bool dermem_open (dermem_size_t master_bytes, void **master) {
// 	return mem_open (master_bytes, master);
// }
#define dermem_open mem_open

/** @brief Close a memory pool.
 *
 * This will free up any application data
 * after first clearing it to zero bytes.  This is the
 * opposite function to dermem_open().
 *
 * The pool does not have to be linked explicitly from the
 * appdata; the appdata is simply the first allocated bit
 * in the data structure, and offset calculations suffice.
 *
 * This function requires a pointer to the master, that it
 * will set to NULL upon return.  If it is NULL already, no
 * cleanup is done.
 *
 * Any resources allocated with dermem_resource() will be
 * zeroed and freed, but first their free_cb() is called
 * with a pointer to the master and slave pointers.
 *
 * This function does not fail.
 */
// static inline void dermem_close (void **master) {
// 	mem_close (master);
// }
#define dermem_close mem_close

/** @brief Clear a memory pool.
 *
 * This is done by zeroing and freeing anything
 * that it had allocated.  Blocks allocated for large
 * requirements are usually custom-sized, so they are
 * freed; blocks with default sizes are simply made
 * empty.  Effectively, the memory is recycled.
 *
 * Callbacks installed in the memory pool are called
 * before the corresponding resources are freed.
 *
 * You can indicate how much memory to keep at most.
 * Set ~0 to keep everything.
 *
 * This function does not fail.
 */
// static inline void dermem_recycle (dermem_size_t keep_at_most, void *master) {
// 	mem_recycle (keep_at_most, master);
// }
#define dermem_recycle mem_recycle


/** @brief Allocate memory to the memory pool.
 *
 * This works on the pool for which the given
 * application data structure serves as the master.  The
 * master was created with dermem_open().  Allocated memory
 * will not be freed until dermem_close() frees the entire
 * memory pool.
 *
 * This function requires that *slave is NULL upon calling.
 * Returned memory is cleared with zero bytes.
 *
 * This function returns true on success, or false on failure.
 * In case of failure, it will have set errno to ENOMEM.
 */
// static inline bool dermem_alloc (void *master, dermem_size_t slave_bytes, void **slave) {
// 	return mem_alloc (master, slave_bytes, slave);
// }
#define dermem_alloc mem_alloc


/** @brief  Allocate memory to the memory pool.
 *
 * This does the same as with dermem_alloc(), but it
 * also sets up a callback function to be called with the
 * memory during dermem_close().
 *
 * Resources will be freed in the reverse order of their
 * insertion with dermem_resource().
 */
// static inline bool dermem_resource (void *master, dermem_size_t slave_bytes, void **slave,
// 			void (*free_cb) (void *master, void *slave)) {
// 	return mem_resource (master, slave_bytes, slave, free_cb);
// }
#define dermem_resource mem_resource


/** @brief Buffer a character string.
 *
 * The input string is provided as a dercursor,
 * so without trailing NUL char, but make it available
 * in the master's pool with a trailing NUL.
 *
 * Before this call, *out_strbuf must be NULL.
 */
// static inline bool dermem_strdup (void *master, dercursor str, char **out_strbuf) {
// 	return mem_strdup (master, crs2buf (str), out_strbuf);
// }
#define dermem_strdup(master,str,out_strbuf) mem_strdup((master), crs2buf (str), (out_strbuf))


/** @brief Allocate a buffer of a given minimum size.
 *
 * After the buffer is allocated, fill it
 * as much as needed.  Then, when done, cap it off and
 * return the remaining bytes to the pool.  You can use
 * this to gradually develop a memory space of variable
 * size, such as a string buffer or, very common, a DER
 * SEQUENCE OF that may not include all elements that
 * you can predict as a worst case.
 *
 * This works by allocating the remainder of a memory
 * chunk for you, and this remaining space is returned
 * to you for continued use.  When closing buffers,
 * their unused portion is returned to the pool.
 *
 * Often, you will not use any other memory during
 * the fillup of the buffer and all that happens is
 * a lowering of the allocated buffer.  But it is
 * not an error to allocate more memory, including
 * these buffers, while at it -- you will just force
 * the allocation of fresh pool blocks if you do so.
 *
 * The buffer should start with derptr set to NULL,
 * and will be set to a derptr not NULL upon success,
 * with derlen the actual buffer size.  You can use
 * all of the buffer, though you don't have to.  Use
 * the same buffer in _close and _resize calls.
 */
// static inline bool dermem_buffer_open (void *master, dermem_size_t min_size, dercursor *buffer) {
// 	return mem_buffer_open (master, min_size, crs2bufp (buffer));
// }
#define dermem_buffer_open(master,min_size,buffer) mem_buffer_open((master), (min_size), crs2bufp (buffer))


/** @brief Cap off a buffer to the desired size.
 *
 * After doing this, clear and return the
 * remaining bytes to the chunk of the pool.  This is
 * only possible for memory allocated with dermem_buffer()
 * and it can only be done once.  It is not a problem
 * however, to use other dermem_ operations at the same
 * time, including other dermem_buffer() operations that
 * have not had dermem_buffer_close() called on them.
 *
 * The freed memory will be zeroed, so having touched
 * it is not a problem.
 *
 * After closing a buffer, you cannot resize it anymore.
 * The derlen in the buffer will be updated nonetheless.
 *
 * This operation does not fail.
 */
// static inline void dermem_buffer_close (void *master, dermem_size_t final_size, dercursor *buffer) {
// 	mem_buffer_close (master, final_size, crs2bufp (buffer));
// }
#define dermem_buffer_close(master,final_size,buffer) mem_buffer_close((master), (final_size), crs2bufp (buffer))


/** @brief Delete a buffer if it is no longer useful.
 *
 * This is just a wrapper around capping off the buffer at a
 * desired size zero.
 *
 * This operation does not fail.
 */
static inline void dermem_buffer_delete (void *master, dercursor *buffer) {
	dermem_buffer_close (master, 0, buffer);
}


/** @brief Allocate a variable-sized data buffer.
 *
 * Resize a buffer to hold at least the desired size, and
 * return the buffer with a possibly updated derptr and
 * derlen.  It is not an error to ask for space already
 * present in the buffer, just to ensure that enough
 * space is indeed available.  Your code should continue
 * with the new derptr as a starting point, possibly by
 * always using the appointed buffer as its start.
 *
 * You can use the entire buffer, even if it is larger
 * than your requested size, but you don't have to.  You
 * must not change the derlen in the buffer, however.
 *
 * This operation may have to reallocate the buffer, so
 * you should not have pointers into the buffer except
 * for offsets and integers.  When reallocating, the
 * old contents are moved into the beginning of the
 * new space.  Unlike the standard function realloc()
 * you can be sure that we will clear the old memory
 * before freeing it.
 *
 * Note that the work involved in these operations is
 * not cheap.  You may want to limit your use to the
 * bare minimum that you can stand, or add comments to
 * suggest performance improvements if you cannot code
 * them immediately.
 */
// static inline bool dermem_buffer_resize (void *master, dermem_size_t min_size, dercursor *buffer) {
// 	return mem_buffer_resize (master, min_size, crs2bufp (buffer));
// }
#define dermem_buffer_resize(master,min_size,buffer) mem_buffer_resize((master), (min_size), crs2bufp (buffer))


/********** EXTENSIONS MADE FOR QUICK-DERMEM **********/


/** @brief Allocate memory and der_pack() a structure into it.
 *
 * Before calling, out_packed->derptr must be NULL.
 */
bool dermem_pack (void *master, const derwalk *syntax, const dercursor *in_cursors,
                                dercursor *out_packed);


/** @brief Pack and send one DER structure over a given socket.
 *
 * Allocate memory, der_pack() a structure into it and send
 * it off over the given socket.
 */
bool dermem_send (void *master, int sox, const derwalk *syntax, const struct dercursor *in_cursors);


/** @brief Receive one DER structure over a given socket and unpack.
 *
 * The data memory for the DER blob is allocated in the master's pool.
 *
 * It assumed that 5 bytes can be read effortlessly.  No more
 * that 16 MB can be read at a time.
 */
bool dermem_recv_der (void *master, int sox, const derwalk *syntax, struct dercursor *out_dercursor,
				struct dercursor *opt_out_opt);


/** @brief Receive one DER structure over a given socket and unpack.
 *
 * The data memory for the DER blob is allocated in the master's pool.
 *
 * It assumed that 5 bytes can be read effortlessly.  No more
 * that 16 MB can be read at a time.
 */
static inline bool dermem_recv (void *master, int sox, const derwalk *syntax, struct dercursor *out_dercursor) {
	return dermem_recv_der (master, sox, syntax, out_dercursor, NULL);
}

#ifdef __cplusplus
}
#endif

#endif

/** @} */

/** @} */
