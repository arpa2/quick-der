# PythonSupport.cmake -- support-macros for using Python cross-platform
#
# This module provides one function for manipulating PYTHONPATH.
# It allows the CMake command-line-parameter PYTHON_EXECUTABLE to
# override the Python3 that is found.
#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>

find_package(Python3 COMPONENTS Interpreter Development REQUIRED)
if (PYTHON_EXECUTABLE)
    # Allow setting the Python executable as part of CMake command-line
    # arguments; mainly useful on Windows. We'll pretend that Python3 is
    # found in that case.
    set(Python3_EXECUTABLE ${PYTHON_EXECUTABLE})
    set(Python3_FOUND TRUE)
endif()

# Not using WIN32 here, because that's set when the **target** system
# is Windows. Here, we need it while building, regardless of target,
# on a Windows host.
if (CMAKE_HOST_SYSTEM MATCHES "Windows")
    set (PYTHON_PATH_SEPARATOR ";")
else()
    set (PYTHON_PATH_SEPARATOR ":")
endif()

# Sets @p VARNAME to the value of the environment-variable PYTHONPATH,
# with @p path appended to it with a suitable separator. If more than
# one value is passed in, they are all appended with suitable separators.
#
# This **could** be generalized, to use a different ENV variable.
function (AppendToPythonPath VARNAME path)
    set (_ppath $ENV{PYTHONPATH})
    # Special-case if the existing environment variable is empty.
    if (NOT _ppath)
        set (_ppath ${path})
    else()
        set (_ppath "${_ppath}${PYTHON_PATH_SEPARATOR}${path}")
    endif()
    # And append all the rest.
    foreach (a ${ARGN})
        set (_ppath "${_ppath}${PYTHON_PATH_SEPARATOR}${a}")
    endforeach()
    
    set (${VARNAME} "${_ppath}" PARENT_SCOPE)
endfunction()
