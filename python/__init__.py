#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Rick van Rein <rick@openfortress.nl>

__import__('pkg_resources').declare_namespace(__name__)
